import midi from './tuning/midi';
import pythagorean from './tuning/pythagorean';

const tuning = {
  'MIDI': midi,
  'Pythagorean': pythagorean
}


window.tuning = tuning;
export default tuning;
