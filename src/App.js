import React, { Component } from 'react';
import hotkeys from 'hotkeys-js';

import MainWindow from './components/mainWindow';
import './App.scss';

import 'react-md/dist/react-md.light_blue-deep_orange.min.css';
import 'material-icons/iconfont/material-icons.scss';

import {Provider} from 'mobx-react';

import PlayStore from './stores/play';
import UIStore from './stores/ui';

class App extends Component {
  constructor() {
    super();
    this.stores = {
      play: new PlayStore(),
      ui: new UIStore()
    }

    Object.keys(this.stores).forEach((store) => {
      this.stores[store].stores = this.stores;
    })
  }

  componentDidMount() {
    hotkeys('f8', (event) => {
      this.stores.ui.toggleKeyboard(false);
    })
  }

  render() {
    return (
      <Provider {...this.stores}>
        <MainWindow />
      </Provider>
    );
  }
}

export default App;
