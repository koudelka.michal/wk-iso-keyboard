import Tone from '../notation';
import AudioEngine from './audio-engine';

class WebMIDIEngine extends AudioEngine{
  constructor(params) {
    super(params)

    this.notesMap = this.buildNoteMap()
    this.output = null;

    this.initialize();
  }

  async initialize() {
    try {
      this.options = {
        channel: 0,
        outputs: [],
        output: null
      };

      this.setOptions(this.options);

      this.midi = await navigator.requestMIDIAccess();
      this.setOption('outputs', this._outputs);
      console.log(this.options);
      if (this.options.outputs.length > 0) {
        this.setOption('output', this.options.outputs[0].value);
      } else {
        throw new Error('no valid output');
      }
      
      this.onReady(); 
       
    } catch(error) {
      console.log(this.onFail)
      this.onFail(error)
    }
  }

  buildNoteMap() {

    let midiNotes = {};
    Tone.all.forEach((tone) => {
        midiNotes[tone.number] = tone.midiTone;
      }
    );

    return midiNotes;
  }

  setNotes(notes) {
    console.log(notes);
    let activeNotes = {};
    
      notes.forEach((note) => {
        let noteKey = this.notesMap[note[0]];
        if (this.activeNotes.hasOwnProperty(noteKey)) {
          if (note[1] !== this.activeNotes[noteKey]) this.sendMessage((0xA0+this.options.channel), noteKey, note[1]) //aftertouch
        } else {
          this.sendMessage((0x90+this.options.channel), noteKey, note[1]); //noteon
        }
        activeNotes[noteKey] = note[1];
      });

      Object.keys(this.activeNotes).forEach((note) => {
        if (!activeNotes.hasOwnProperty(note)) this.sendMessage((0x80+this.options.channel), note, 0); //note off
      });
      this.activeNotes = activeNotes;
  }

  sendMessage(b1,b2,b3) {
    this.output.send([b1,b2,b3]);
  }

  unload() {
    return true
  }

  optionChanged(key,value) {
    super.optionChanged(key,value);
    switch(key) {
      case 'output':
        this.output = this.midi.outputs.get(value);
        break;
      default:
        break;
    }
  }

  get _outputs() {
    console.log(this.midi.outputs);
    let outputs = [];
    this.midi.outputs.forEach((output) => {
      outputs.push({label: output.name, value: output.id});
    });
    console.log(outputs)
    return outputs;
  }
  

  static get available() {
    return Boolean(navigator.requestMIDIAccess);
  }

  static get name() {
    return "Webmidi";
  }

  static get key() {
    return "webmidi";
  }

  static get supportTuning() {
    return false; 
  }
}

export default WebMIDIEngine;
