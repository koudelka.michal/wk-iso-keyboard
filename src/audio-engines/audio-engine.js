
export default class {
  constructor(params) {
    console.log(params);
    this.onFail = params.onFail;
    this.onReady = params.onReady;
    this.setOptions = params.setOptions;
    this.setOption = params.setOption;
    this.activeNotes = {};
    this.options = {}
  }
  
  optionChanged(key,value) {
    this.options[key] = value
    return true;
  }
}
