import ToneJS from 'tone';
import AudioEngine from './audio-engine';

class ToneJSEngine extends AudioEngine {
  constructor(params) {
    super(params)
    this.tuningTable = {}

    this.options = {
      polyphony: 4,
      oscillator: 'triangle',
      attack: 0.005,
      decay: 0.1,
      sustain: 0.3,
      release: 1,
    }

    this.initialize();
  }

  setTuning(tuningTable) {
    this.tuningTable = tuningTable;
    this.synth = new ToneJS.PolySynth().toMaster()
    this._updateSynthOptions();
  }
  async initialize() {
    this.setOptions(this.options);
    try {
      this.onReady(); 
    } catch(error) {
      this.onFail(error)
    }
  }

  setChannel(channel) {
    this.channel = channel;
  }

  getOutputs() {
    return this.outputs.map((output) => { return {id: output.id, name: output.name}});
  }

  setNotes(notes) {
    let activeNotes = {};
    
      notes.forEach((note) => {
        let noteKey = note[0];
        if (this.activeNotes.hasOwnProperty(noteKey)) {
          //if (note[1] !== this.activeNotes[noteKey]) this.synth.triggerAttackRelease() //aftertouch
        } else {
          this.synth.triggerAttack(this.tuningTable[noteKey], undefined, 1); //noteon
        }
        activeNotes[noteKey] = note[1];
      });

      Object.keys(this.activeNotes).forEach((note) => {
        if (!activeNotes.hasOwnProperty(note)) this.synth.triggerRelease(this.tuningTable[note]); //note off
        if (!activeNotes.hasOwnProperty(note)) console.log('note off', note); //note off
      });
      this.activeNotes = activeNotes;
  }

  optionChanged(key,value) {
    super.optionChanged(key,value);
    const synthOptions = ['oscillator', 'attack', 'decay', 'sustain', 'release'];
    if (synthOptions.indexOf(key) !== -1) {
      this._updateSynthOptions();
    }
  }

  _updateSynthOptions() {
    this.synth.set({
      envelope: {
        attack: this.options.attack,
        sustain: this.options.sustain,
        delay: this.options.delay,
        release: this.options.release
      },
      oscillator: {
        type: this.options.oscillator
      }
    }); 
  }

  unload() {
    return true
  }

  static available() {
    return Boolean(window.AudioContext);
  }

  static get name() {
    return "Tone.JS";
  }

  static get key() {
    return "tonejs";
  }

  static get supportTuning() {
    return true
  }
}

export default ToneJSEngine;
