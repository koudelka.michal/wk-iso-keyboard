import React from 'react';

import Keyboard from './keyboard';
import Loader from './loader';
import Settings from './settings';
import ControlPanel from './control-panel';

import {inject,observer} from 'mobx-react';


@inject('ui')
@observer
class MainWindow extends React.Component {
  render() {
    return(
      <div className="mainWindow App" id="mainWindow" style={{width:'100vw',height:'100vh',position:'relative'}}>
        {this.props.ui.showKeyboard && <Keyboard />}
        {this.props.ui.showSettings && <Settings />}
        {this.props.ui.loading && <Loader />}
        {this.props.ui.showControlPanel && <ControlPanel />}
      </div>
    );
  }
}

export default MainWindow;
