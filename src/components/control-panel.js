import React from 'react';
import { observer,inject } from 'mobx-react';
import {DialogContainer, Button, Toolbar} from 'react-md';

@inject('play')
@inject('ui')
@observer
class ControlPanel extends React.Component {
  render() {
    return (
      <DialogContainer
        modal
        containFocus 
        contentStyle={{height:'100%', overflow: 'visible'}} 
        id="engine-control-panel"
        fullPage
        visible
        actions={[
        ]}
      >
        <Toolbar
          colored
          title="Control Panel"
          actions={
            [
              <Button icon onClick={() => {this.props.ui.toggleControlPanel(false)}}>close</Button>
            ]
          }
        />
        <this.props.play.controlPanelComponent 
          options={this.props.play.engineOptions}
          setOption={(key,value) => {this.props.play.engine.setOption(key,value)}}
        />
      </DialogContainer>
    )
  }
}

export default ControlPanel;
