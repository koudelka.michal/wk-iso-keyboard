import React from 'react';
import { observer,inject } from 'mobx-react';
import { Button, Card, CardActions, CardText, CardTitle, SelectField, TextField, Toolbar } from 'react-md';

import Tone, {basicTones, rootTones} from '../notation';
import Keyboards from './keyboard/keyboards';

import {debounce} from 'lodash';

@inject('play')
@inject('ui')
@observer
class Settings extends React.Component {

  get octaves(){
    return [ 1,2,3,4,5,6,7,8,9];
  }

  get channels() {
    return [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
  }

  render() {
    return(
      <div className="settingsComponent">
        <Toolbar
          colored
          title="Settings"
          actions={
            [
              <Button
                key="toggle-fullscreen"
                icon
                onClick={() => {this.props.ui.toggleFullscreen()}}
              >{(this.props.ui.isFullscreen) ? 'fullscreen_exit' : 'fullscreen'}</Button>
            ]
          }
        />
        <div>
          <Card>
            <CardTitle>Engine</CardTitle>
            <CardText>
              <SelectField 
                placeholder="Midi Subsystem"
                id="settings-midi-subsystem"
                label="Midi Subsystem"
                value={this.props.play.engineKey}
                menuItems={this.props.play.engines.map((e, i) => { return { value: e.key, label: e.name }})}
                className="md-cell md-cell--bottom"
                onChange={(e) => {this.props.play.initEngine(this.props.play.engines.find((engine) => {return engine.key === e}))}}
              />
              <SelectField 
                placeholder="Tuning"
                disabled={!this.props.play.engine.constructor.supportTuning}
                id="settings-midi-subsystem"
                label="Tuning"
                value={this.props.play.tuning}
                menuItems={this.props.play.tunings.map((t,i) => { return { value: t, label: t }})}
                className="md-cell md-cell--bottom"
                onChange={(e) => {this.props.play.setTuning(this.props.play.tunings.find((tuning) => {return tuning === e}))}}
              />
              <SelectField 
                placeholder="Base tone"
                disabled={!this.props.play.engine.constructor.supportTuning}
                id="settings-tuning-tone"
                label="Base tone"
                value={this.props.play.tuningTone}
                menuItems={Tone.all.filter((t) => { return rootTones.indexOf(t.basicTone) !== -1}).map((t) => { return { value: t.number, label: t.label }})}
                className="md-cell md-cell--bottom"
                onChange={(e) => {this.props.play.setTuningTone(e)}}
              />
              <TextField 
                placeholder="Base tone frequency"
                disabled={!this.props.play.engine.constructor.supportTuning}
                id="settings-tuning-frequency"
                label="Base tone frequency"
                value={this.props.play.tuningFrequency}
                className="md-cell md-cell--bottom"
                onChange={debounce((e) => {this.props.play.setTuningFrequency(e)}, 150)}
                type="number"
                defaultValue="number"
              />
            </CardText>
            {this.props.play.controlPanelAvailable &&
              <CardActions>
                <Button flat secondary onClick={() => {this.props.ui.toggleControlPanel()}}>Show control panel</Button>
              </CardActions>
            }
          </Card>
        </div>
        <div>
          <Card>
            <CardTitle>Keyboard</CardTitle>
            <CardText>
              <SelectField 
                placeholder="Base Tone"
                id="settins-keyboard-base-tone"
                label="Base Tone"
                value={this.props.play.baseTone}
                menuItems={rootTones.map((note) => { return { value: note, label: basicTones[note] }})}
                className="md-cell md-cell--bottom"
                onChange={(tone) => {this.props.play.setBaseTone(tone)}}
              />
              <SelectField 
                placeholder="Base octave"
                id="settings-keyboard-base-octave"
                label="Base Octave"
                value={this.props.play.baseOctave}
                menuItems={this.octaves}
                className="md-cell md-cell--bottom"
                onChange={(octave) => {this.props.play.setBaseOctave(octave)}}
              />
              <SelectField 
                placeholder="Type"
                id="settings-keyboard-type"
                label="Type"
                value={this.props.play.keyboardType}
                menuItems={Keyboards.map((keyboard) => {return { label: keyboard.label, value: keyboard.value }})}
                className="md-cell md-cell--bottom"
                onChange={(keyboardType) => {this.props.play.setKeyboardType(keyboardType)}}
              />
            </CardText>
          </Card>
        </div>
        <div>
          <Button flat secondary swapTheming onClick={() => this.props.ui.toggleKeyboard(true) }>Close settings</Button>
        </div>
      </div>
    );
  }
}


export default Settings
