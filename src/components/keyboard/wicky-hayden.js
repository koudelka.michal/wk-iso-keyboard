import Tone, {intervals} from '../../notation';

//Obsolete, make it nicer!
export default function({octaves, baseTone, baseOctave}) {
  let keys = [];
  for (let i = 0 ; i < octaves ;i++) {
    let ir = (octaves-1)-i;
    let octave = baseOctave-(Math.round(octaves/2))+i+1;
    for (let k = 0;k < 19;k++) {
      let key = { position: {}};
      if (k >8) {
        key.position.x = (k-9);
        key.position.y = (ir*2)+1;
        key.tone = new Tone((octave*40)+baseTone).increase(intervals.P4).decrease(intervals.M2,3).increase(intervals.M2, k-9);
      } else {
        key.position.x = k+0.5;
        key.position.y = (ir*2)+2;
        key.tone = new Tone((octave*40)+baseTone).decrease(intervals.M2 ,3).increase(intervals.M2, k);
      }
      //if (this.props.play.baseTone === 0) key.tone -= 12; 
      key.id=`key-${key.position.x}-${key.position.y}`;
      keys.push(key);
    }
  }
  return keys;
 
}
