import makeWickyHayden from './wicky-hayden';
import makeWickyHaydenPentatonic from './wicky-hayden-pentatonic';

const keyboards = [
  {
    value: 'wh',
    label: 'Wicky Hayden',
    generator: makeWickyHayden,
    width: 10
  },
  {
    value: 'whp',
    label: 'Pentatonic',
    generator: makeWickyHaydenPentatonic,
    width: 3
  }
];

export default keyboards;
