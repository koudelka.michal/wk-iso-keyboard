import WebMIDIControlPanel from './webmidi/control-panel';
import ToneJSControlPanel from './tone/control-panel';

export default {
  webmidi: WebMIDIControlPanel,
  tonejs: ToneJSControlPanel
}
