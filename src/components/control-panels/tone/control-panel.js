import React from 'react';
import { SelectField, Card, CardTitle, CardText, Slider, Grid, Cell} from 'react-md';

const oscillators = [
  {
    value: 'sine',
    label: 'Sinwave'
  },
  {
    value: 'triangle',
    label: 'Triangle'
  },
  {
    value: 'sawtooth',
    label: 'Sawtooth'
  },
  {
    value: 'square',
    label: 'Square'
  },
  {
    value: 'pwm',
    label: 'Pulsewave modulatoin'
  }
]

export default function({options,setOption}) {
  console.log('rendered');
  return(
    <div>
      <Card>
        <CardTitle>Oscillator settings</CardTitle>
        <CardText style={{height:'100%'}}>
          <SelectField
            placeholder="Waveform"
            label="Waveform"
            id="tonejs-control-panel-oscillator"
            value={options.oscillator}
            menuItems={oscillators}
            onChange={(value) => {setOption('oscillator', value)}}
            className="md-cell md-cell--bottom"
          />
        </CardText>
      </Card>
      <Card>
        <CardTitle>ASDR Envelope</CardTitle>
        <CardText>
          <Grid>
            <Cell size={3}>
              <Slider
                label="Attack (ms)"
                id="tonejs-control-panel-attack"
                min={0}
                max={2000}
                step={1}
                editable
                value={options.attack*1000}
                onChange={(value) => {setOption('attack', value/1000)}}
              />
            </Cell>
            <Cell size={3}>
              <Slider
                label="Sustain (ms)"
                id="tonejs-control-panel-sustain"
                min={0}
                max={2000}
                step={1}
                editable
                value={options.sustain*1000}
                onChange={(value) => {setOption('sustain', value/1000)}}
              />
            </Cell>
            <Cell size={3}>
              <Slider
                label="Decay (ms)"
                id="tonejs-control-panel-decay"
                min={0}
                max={2000}
                step={1}
                editable
                value={options.decay*1000}
                onChange={(value) => {setOption('decay', value/1000)}}
              />
            </Cell>
            <Cell size={3}>
              <Slider
                label="Release (ms)"
                id="tonejs-control-panel-release"
                min={0}
                max={5000}
                step={1}
                editable
                value={options.release*1000}
                onChange={(value) => {setOption('release', value/1000)}}
              />
            </Cell>
          </Grid>
        </CardText>
      </Card>
    </div>
  )
}
