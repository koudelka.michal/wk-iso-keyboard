import React from 'react';
import { SelectField, Card, CardTitle, CardText } from 'react-md';

export default function({options,setOption}) {
  return(
    <Card>
      <CardTitle>Web midi settings</CardTitle>
      <CardText style={{height:'100%'}}>
        <SelectField
          placeholder="Output"
          label="Output"
          id="webmidi-control-panel-output"
          value={options.output}
          menuItems={options.outputs}
          onChange={(value) => {setOption('output', value)}}
          className="md-cell md-cell--bottom"
        />
        <SelectField
          label="Channel"
          placeholder="Channel"
          id="webmidi-control-panel-channel"
          value={options.channel}
          menuItems={[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]}
          onChange={(value) => {setOption('channel', value)}}
          className="md-cell md-cell--bottom"
        />
      </CardText>
    </Card>
  )
}
