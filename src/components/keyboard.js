import React from 'react';
import {inject, observer} from 'mobx-react';
import {find} from 'lodash';
import keyboards from './keyboard/keyboards';

@inject('play')
class Key extends React.Component {

  get posX() {
    return ((Math.sqrt(3)*this.props.size)*this.props.x)+(Math.sqrt(3)*this.props.size)/2;
  }

  get posY() {
    return  this.props.size*1.5*this.props.y;
  }
  
  render() {
    return(
      <g className="keyButton" data-semitone={this.props.isSemitone} >
        <use
          x={(this.posX)}
          y={this.posY}
          data-x={this.props.x}
          style={this.props.css}
          data-y={this.props.y}
          href="#hexagon"
          data-tone={this.props.tone}
        />
        <text
          x={(this.posX-30)}
          y={this.posY-15}
          style={{userSelect:'none', pointerEvents:'none'}}
        >{this.props.label}</text>
      </g>
    );
  }
}

@inject('play')
@observer
class Keyboard extends React.Component {
 
  constructor() {
    super();
    this.keyWidth = 50;
    this.rootRef = React.createRef();

    this.handleTouch = (event) => {
      event.preventDefault();
      let notes = [];

      for (let i=0; i< event.touches.length;i++) {
        let touch = event.touches.item(i);
        if (touch) {
          //let force = touch.force || 1;
          let force = 1;
          let element = document.elementFromPoint(touch.clientX,touch.clientY);
          if (element && element.hasAttribute('data-tone')) notes.push([parseInt(element.getAttribute('data-tone')),Math.ceil(127*force)]);
        }
      }
      this.props.play.setNotes(notes);
    };
  }

  get hexagonPoints() {
    let hexagonPoints = [];

    for (var i=0; i<=5;i++) {
      let px = Math.round(0 + this.keyWidth * Math.sin(i * (Math.PI * 2) / 6))
      let py = Math.round(0 + this.keyWidth * Math.cos(i * (Math.PI * 2) / 6)) 
      hexagonPoints.push(`${px},${py}`);
    }

    return hexagonPoints;
  }

  get keyboardType() {
    return find(keyboards, {value: this.props.play.keyboardType})
  }

  get viewBox() {
    return `0 0 ${this.width} ${this.height}`;
  }

  get width() {
    return Math.floor((this.keyboardType.width*(Math.sqrt(3)*this.keyWidth)));
  }

  get height() {
    return (this.props.play.octaves*2+1)*this.keyWidth*1.5;
  }



  componentDidMount() {
    this.rootRef.current.addEventListener('touchstart', this.handleTouch, {passive: false});
    this.rootRef.current.addEventListener('touchmove', this.handleTouch);
    this.rootRef.current.addEventListener('touchend', this.handleTouch);
  }

  render() {
    let keys = this.keyboardType.generator({
      octaves: this.props.play.octaves,
      baseOctave: this.props.play.baseOctave,
      baseTone: this.props.play.baseTone
    });
    console.log(keys);
    return(
      <svg
        viewBox={this.viewBox}
        style={{width:'100%',height:'100%',position:'absolute',top:0,left:0, backgroundColor: 'black'}}
        width={this.width}
        height={this.height}
        onContextMenu={(event) => { event.preventDefault()}}
        ref={this.rootRef}
        className="keyboardComponent"
      >
        <defs>
          <polygon id="hexagon" points={this.hexagonPoints.join(' ')} className="hexagon" />
        </defs>
          {keys.map((key) => {
            return (
              <Key
                x={key.position.x}
                y={key.position.y}
                size={this.keyWidth}
                tone={key.tone.number}
                label={key.tone.label}
                key={key.id}
                css={key.style}
                isSemitone={key.tone.isSemitone}
              />
            )
          })}
      </svg>
    )
  }
}

export default Keyboard;
