import Tone from '../notation'
import fixDecimals from './fixDecimals';

function midiTuning(baseTone=192, frequency=440, decimals=3) {
  const pitches = {}
  const baseToneMidiNumber = new Tone(baseTone).midiTone;

  Tone.all.forEach((tone) => {
    pitches[tone.number] = fixDecimals(frequency * Math.pow(2.0, (tone.midiTone - baseToneMidiNumber)/12), decimals)
  });

  return pitches;
}

export default midiTuning;
