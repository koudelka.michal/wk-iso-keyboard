export default (frequency, decimals) => parseFloat(frequency.toFixed(decimals));
