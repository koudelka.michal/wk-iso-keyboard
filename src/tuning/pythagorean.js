import Tone, {intervals} from '../notation';
import fixDecimals from './fixDecimals';

function pythagoreanTuning(baseTone=192,frequency=440, decimals=3) {
  const pitches = {};
  Tone.all.forEach((tone) => {
    pitches[tone.number] = null;
  });

  const intervalSteps = {
    'M2': (9/8),
    'P4': (4/3),
    'P5':  (3/2)
  }

  pitches[baseTone] = fixDecimals(frequency, decimals);

  let calculated = [];
  let finished = false;
  do {
    let ci = 0;
    Object.keys(pitches).forEach((tone) => {
      tone = parseInt(tone);
      if (pitches[tone] !== null && calculated.indexOf(tone) === -1) {
        const pitch = pitches[tone];

        const toneObject = new Tone(tone);

        ['M2', 'P4','P5'].forEach((interval) => {
          const higherTone = toneObject.increase(intervals[interval]);
          const lowerTone = toneObject.decrease(intervals[interval]);

          if (higherTone.isValid && (pitches[higherTone.number] === null)) {
            pitches[higherTone.number] = fixDecimals(pitch*intervalSteps[interval], decimals)
          }
          
          if (lowerTone.isValid && (pitches[lowerTone.number] === null)) {
            pitches[lowerTone.number] = fixDecimals(pitch/intervalSteps[interval], decimals);
          }
        });

        calculated.push(tone);
        ci++;
      }
    });
    if (ci === 0) finished = true;
  }
  while (!finished);

  return pitches;
}

window.pt = pythagoreanTuning;
export default pythagoreanTuning;
