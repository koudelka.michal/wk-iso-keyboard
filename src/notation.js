const basicTones = {
  1: 'C♭♭', 2: 'C♭', 3: 'C', 4: 'C♯', 5: 'C♯♯',
  7: 'D♭♭', 8: 'D♭', 9: 'D', 10:'D♯', 11:'D♯♯',
  13: 'E♭♭', 14: 'E♭', 15:'E', 16: 'E♯', 17: 'E♯♯',
  18: 'F♭♭', 19: 'F♭', 20: 'F', 21:'F♯', 22: 'F♯♯', 
  24: 'G♭♭', 25: 'G♭', 26: 'G', 27: 'G♯', 28: 'G♯♯',
  30: 'A♭♭', 31:'A♭', 32: 'A', 33: 'A♯', 34: 'A♯♯',
  36:'B♭♭', 37:'B♭', 38:'B', 39: 'B♯', 40: 'B♯♯',
};

const midiTones = [
  [3, 7, 39], //C
  [4, 8, 40], //C#
  [5, 9, 13], //D
  [10, 14, 18], //D#
  [15, 11, 19], //E
  [20, 16, 24], //F
  [25, 21, 17], //Gb
  [26, 22, 30], //G
  [31, 27], //Ab
  [32, 36, 28], //A
  [37, 33, 1], //Bb
  [38, 34, 2], //B
];

const midiLabels = ['C', 'C♯', 'D', 'D♯', 'E', 'F', 'G♭', 'G', 'A♭', 'A', 'B♭', 'B']
const rootTones = [3, 9, 15, 20, 26, 32, 38];
const sharps = [4, 10, 21, 27, 33, 40]
const flats = [1, 8, 14, 18, 25, 31, 37]

const intervals = {
  M2: 6,
  P4: 17,
  P5: 23
}

const Tone = class {
  constructor(toneNumber) {
    this.toneNumber = toneNumber;
  }

  increase(interval, steps=1) {
    return new this.constructor(this.toneNumber + (interval*steps));
  }

  // A bit of syntax sugar
  decrease(interval,steps=1) {
    return this.increase(interval,steps*-1)
  }

  get label() {
    return `${basicTones[this.basicTone]}${this.octave}`; 
  }

  get basicTone() {
    let toneIndex = this.toneNumber % 40;
    return (toneIndex !== 0) ? toneIndex : 40;
  }

  get octave() {
    return Math.floor((this.toneNumber / 40)); 
  }

  get isFlat() {
    return Boolean(flats.indexOf(this.basicTone) !== -1);
  }
  
  get isSharp() {
    return Boolean(sharps.indexOf(this.basicTone) !== -1);
  }

  get isSemitone() {
    return this.isFlat || this.isSharp;
  }

  get number() {
    return this.toneNumber;
  }

  get isValid() {
    return (this.toneNumber > 0 && this.toneNumber <= 360)
  }
  get midiTone() {
    let midiTone;

    for (let i in midiTones) {
      if (midiTones[i].indexOf(this.basicTone) !== -1) {
        midiTone = parseInt(i)+(12*this.octave);
        if (this.basicTone > 2) midiTone += 12;
        if (this.basicTone > 38) midiTone += 12;
        break;
      }
    }
    return midiTone;
  }
  
  get midiLabel() {
    let key = this.midiTone % 12;
    let octave = Math.floor(this.midiTone/12);

    return `${midiLabels[key]}${octave}`;
  }

  static get all() {
    let baseTones = Object.keys(basicTones);
    let tones = [];

    for (let octave=0;octave<=9;octave++) {
      baseTones.forEach((baseTone) => {
        let fullTone = parseInt(baseTone) + (40*octave);
        tones.push(new this(fullTone));
      });
    }
    return tones;
  }
}

window.Tone = Tone;

export default Tone;

export {
  basicTones,
  rootTones, 
  intervals
}
