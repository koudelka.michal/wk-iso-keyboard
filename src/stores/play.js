import {action, computed, observable} from 'mobx';
import WebMIDIEngine from '../audio-engines/webmidi';
import ToneJSEngine from '../audio-engines/tone';
import Tunings from '../tuning';

import ControlPanels from '../components/control-panels/control-panels';

class PlayStore {

  @observable outputs = [];
  @observable output = null;
  @observable midiChannel = 0;
  @observable activeNotes = [];
  @observable engines = [];
  @observable tunings = [];
  @observable engine = null;
  @observable tuning = 'MIDI';
  @observable keyboardType = 'wh'; //Use wicky hayden by default

  @observable tuningTone = 192;
  @observable tuningFrequency = 440

  @observable octaves = 3;
  @observable baseTone = 3; // C
  @observable baseOctave = 4;
  @observable engineieady = false;
  @observable engineOptions = {};

  constructor() {
    window.play = this;
    this.initEngines();
  }

  @action toggleReady(value) {
    this.engineReady=value;
    if (value) this.engineFailed = false;
  }

  @action toggleFailed(err=null) {
    this.engineFailed=true;
    this.engineReady=false;
    if (err) {
      console.log(err);
    }
  }

  @action setBaseOctave(baseOctave) {
    this.baseOctave = baseOctave;
  }

  @action setKeyboardType(keyboardType) {
    this.keyboardType = keyboardType;
  }

  @action initEngines() {
    this.engines = [];
    if (WebMIDIEngine.available) this.engines.push(WebMIDIEngine);
    if (ToneJSEngine.available) this.engines.push(ToneJSEngine);
    if (this.engines.length > 0) {
      this.initEngine(this.engines[0]);
    }

    this.tunings = Object.keys(Tunings);
    this.tuning = this.tunings[0];
  }
  
  @action initEngine(engineClass) {
    const params = {
      onReady: () => { this.toggleReady(true)},
      onFail: (err) => { this.toggleFailed(err)},
      setOptions: (options) => { this.setEngineOptions(options)},
      setOption: (key, value) => { this.setEngineOption(key,value)}
    }
    this.engine = new engineClass(params);
    this._retune();
  }

  @action setTuning(tuning) {
    this.tuning = tuning;
    this._retune();
  }

  @action setTuningTone(tone) {
    this.tuningTone = tone;
    this.tuningFrequency = Tunings['MIDI']()[tone];
    this._retune();
  }

  @action setTuningFrequency(frequency) {
    this.tuningFrequency = parseFloat(frequency);
    this._retune();
  }


  @action setBaseTone(baseTone) {
    this.baseTone = baseTone;
  }

  // Use when engine options are initiated for a first time;
  @action setEngineOptions(options) {
    this.engineOptions = Object.assign({}, options);
  }

  // Use when specific field is updated, e.g. from control panel
  @action setEngineOption(key,value) {
    const newOptions = this.engineOptions = Object.assign({}, this.engineOptions)
    newOptions[key] = value;
    this.engineOptions = newOptions;
    this.engine.optionChanged(key,value)
  }

  @action async unloadtMidiEngine() {
    await this.midiEngine.unload();
  }

  @action setNotes(notes) {
    this.engine.setNotes(notes);
  }

  @computed get engineKey() {
    try {
      return this.engine.constructor.key;
    } catch(e) {
      return null;
    }
  }

  @computed get controlPanelAvailable() {
    return Boolean(ControlPanels[this.engine.constructor.key]);
  }

  @computed get controlPanelComponent() {
    return ControlPanels[this.engine.constructor.key];
  }

  _retune() {
      console.log(this.tunings);
      if (this.engine.constructor.supportTuning) {
        this.engine.setTuning(Tunings[this.tuning](this.tuningTone, this.tuningFrequency)); 
      }
  }
}

export default PlayStore;
