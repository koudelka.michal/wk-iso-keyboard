import {action, observable} from 'mobx';

const isFullscreen = function() {
  let fullscreenElement = document.fullscreenElement || document.webkitFullscreenElement;

  return (fullscreenElement !== null);
}

class UIStore {
  @observable loading = false;
  @observable showKeyboard = false;
  @observable showSettings = true;
  @observable isFullscreen = isFullscreen();
  @observable showControlPanel = false;

  constructor() {
    ["fullscreenchange", "webkitfullscreenchange", "mozfullscreenchange", "msfullscreenchange"].forEach((eventType) => {
      document.addEventListener(eventType,() => {
        this.setFullscreen(isFullscreen())
      }, false) 
    });
    window.ui = this;
  }

  @action toggleKeyboard(value=true) {
    this.showSettings = !value;
    this.showKeyboard = value;
  }

  @action toggleControlPanel(value=true) {
    this.showControlPanel=value;
  }
  
  @action showLoading() {
    this.loading = true;
  }

  @action toggleFullscreen() {
    const requestFullscreen = document.body.requestFullscreen || document.body.webkitRequestFullscreen;
    const exitFullscreen = document.exitFullscreen || document.webkitExitFullscreen;
    
    if (!isFullscreen()) {
      requestFullscreen.call(document.getElementById('mainWindow'));
    } else {
      exitFullscreen.call(document);
    }
  }

  @action setFullscreen(value) {
    this.isFullscreen = value;
  }
}

export default UIStore;
